import os
import sys

source_root = os.path.join(os.path.dirname(os.path.abspath(__file__)), "app/")
sys.path.insert(0, source_root)
