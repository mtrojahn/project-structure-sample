# Exemplo de estrutura de projeto

Com esta estrutura de diretórios e as configurações no pyproject.toml e no conftest.py é possível rodar o projeto sem muitos problemas de import.

Do diretorio principal do projeto rode:

Para a aplicaçao: `python main.py`

Para os testes: `pytest tests/`

---

Nesta estrutura, no código normal ou nos testes, sempre importe outros módulos usando como base o `app`, ou seja:

```
from app.service import echo
from app.another_service import echo2
```



