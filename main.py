from app.service import echo
from app.another_service import echo2


if __name__ == "__main__":
    print(echo("echo called"))
    print(echo2("echo2 called"))
